function addTitleLayer(map){
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data <a target="_blank" href="http://www.openstreetmap.org">OpenStreetMap.org</a> contributors, ' +
        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
    }).addTo(map);
}


(function ( $ ) {
 
    $.fn.createMarker = function(hotel) {
        var _id = $(this).attr('id');
        var map = L.map(_id).setView([hotel.geoLat, hotel.geoLong], 16);
        addTitleLayer(map);

        function cretePopupContent(hotel) {
            console.error('Тут то, что в попапе выводится. Текст или html')
            return "<b>Hello world!</b><br>I am a popup."
        }
        
        var marker = L.marker([hotel.geoLat, hotel.geoLong]).addTo(map);
        marker.bindPopup(cretePopupContent)
        return this;
    };


    $.fn.createCluster = function(options) {
        if (options === void 0 ) {
            options = {
                mapView: [0, 0],
                size: 16,
                hotels: []
            };
        }
        var _id = $(this).attr('id');
        var map = L.map(_id).setView(options.mapView, options.size);
        addTitleLayer(map);
        
        function onEachFeature(feature, layer) {
            console.error('Тут то, что в попапе выводится. Текст или html. Ниже привязка свойства объекта')
            if (feature.properties && feature.properties.popupContent) {
                layer.bindPopup(feature.properties.popupContent);
            }
        }

        var geojsonFeature = [];
        
        options.hotels.forEach(hotel => {
            geojsonFeature.push({
                type: "Feature",
                properties: {
                    popupContent: hotel.descriptionRu
                },
                geometry: {
                    type: "Point",
                    coordinates: [hotel.geoLat, hotel.geoLong]
                }
            });
        });

        L.geoJSON(geojsonFeature, {
            onEachFeature: onEachFeature
        }).addTo(map);
        return this;
    }
 
}( jQuery ));